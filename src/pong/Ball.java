package pong;

import java.awt.*;
import java.util.Random;

public class Ball extends Rectangle {
    Random random;
    int xVelocity;
    int yVelocity;
    int initialSpped = 3;   // speeding up the ball factor

    public Ball(int x, int y, int ballWidth, int ballHeight) {
        super(x, y, ballWidth, ballHeight);
        random = new Random();
        int randomXDirection =  random.nextInt(2);   // 0 or 1 (left or right)
        if (randomXDirection == 0) {
            randomXDirection--;                             // negative now
        }
        setXDirection(randomXDirection * initialSpped);
        int randomYDirection =  random.nextInt(2);   // 0 or 1 (up or down)
        if (randomYDirection == 0) {
            randomYDirection--;                             // negative now
        }
        setYDirection(randomYDirection * initialSpped);

    }

    public void setXDirection(int randomXDirection) {
        xVelocity = randomXDirection;
    }

    public void setYDirection(int randomYDirection) {
        yVelocity = randomYDirection;
    }

    public void move() {
        x += xVelocity;
        y += yVelocity;
    }

    public void draw(Graphics g) {
        g.setColor(Color.WHITE);
        g.fillOval(x, y, width, height);
    }

}
