package pong;

import javax.swing.*;
import java.awt.*;

public class GameFrame extends JFrame  {
    GamePanel panel;

    public GameFrame() {
        this.panel = new GamePanel();
        this.add(panel);
        this.setTitle("Ever lasting PONG game");
        this.setResizable(false);                               // cannot be resized
        this.setBackground(Color.BLACK);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);                                  // show frame
        this.setLocationRelativeTo(null);                       // middle ot the screen
    }
}
