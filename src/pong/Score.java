package pong;

import java.awt.*;

public class Score extends Rectangle {
    static int GAME_WIDTH;
    static int GAME_HEIGHT;
    int playerOne;
    int playerTwo;

    public Score(int gameWidth, int gameHeight) {
        GAME_WIDTH = gameWidth;
        GAME_HEIGHT = gameHeight;
    }

    public void draw(Graphics g) {
        g.setColor(Color.WHITE);
        g.setFont(new Font("Consolas", Font.PLAIN, 60));
        // middle line
        g.drawLine(GAME_WIDTH/2, 0, GAME_WIDTH/2, GAME_HEIGHT);
        // scores
        g.drawString(String.valueOf(playerOne/10) + String.valueOf(playerOne%10),
                      (GAME_WIDTH/2)-95, 50);
        g.drawString(String.valueOf(playerTwo/10) + String.valueOf(playerTwo%10),
                      (GAME_WIDTH/2)+20, 50 );
    }
}
